<?php

use App\Config\EnvReader;
use App\DatabaseInstaller;
use App\Provider\Mysql;

require_once "autoloader.php";
autoloader::register();

$configProvider = new EnvReader(configFilePath: '.env');
$connection = Mysql::initMysqlConnection($configProvider);

$dbInstaller = new DatabaseInstaller($connection, $configProvider);

$dbInstaller->install();