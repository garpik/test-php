<?php

namespace App\Config;

use App\BaseModels\DataObject;
use Exception;

class EnvReader extends DataObject
{
    /**
     * @param string $configFilePath
     * @param array $data
     * @throws Exception
     */
    public function __construct(
        protected array           $data = [],
        protected readonly string $configFilePath = '.env'
    )
    {
        parent::__construct($data);
        $this->init();
    }

    /**
     * @return void
     * @throws Exception
     */
    private function init(): void
    {
        try {
            $initFIleData = parse_ini_file($this->configFilePath);
            $this->setData(value: is_array($initFIleData) ? $initFIleData : []);
        } catch (Exception $e) {
            throw new Exception("Can't open config file");
        }
    }

}