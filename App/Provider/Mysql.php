<?php

namespace App\Provider;

use App\Config\EnvReader;
use PDO;
use PDOStatement;

class Mysql
{

    private static $connection = null;
    /**
     * @var PDO
     */
    private PDO $pdoConnection;

    /**
     * @param $host
     * @param $database
     * @param $user
     * @param $password
     */
    public function __construct(
        protected $host,
        protected $database,
        protected $user,
        protected $password
    )
    {
        $this->pdoConnection = new PDO("mysql:host=$this->host;dbname=$this->database", $this->user, $this->password);
    }

    /**
     * @param EnvReader $configProvider
     * @return static|null
     */
    public static function initMysqlConnection(EnvReader $configProvider)
    {
        if (self::$connection === null) {
            self::$connection = new static($configProvider->getMysqlHost(), $configProvider->getMysqlDatabase(), $configProvider->getMysqlUser(), $configProvider->getMysqlPassword());
        }
        return self::$connection;
    }

    /**
     * @return Mysql
     */
    public static function getInstance(): Mysql
    {
        return self::$connection;
    }

    /**
     * @param $query
     * @param $bind
     * @param $options
     * @return false|PDOStatement
     */
    public function execute($query, $bind = [], $options = [])
    {
        $preparedStatement = $this->pdoConnection->prepare($query, $options);
        $preparedStatement->execute($bind);
        return $preparedStatement;
    }


}