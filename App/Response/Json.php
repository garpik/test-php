<?php

namespace App\Response;

use App\Interfaces\ResponseInterface;
use JetBrains\PhpStorm\NoReturn;

class Json implements ResponseInterface
{

    protected $data = [];

    /**
     * @param array $data
     * @return Json
     */
    public function setData(array $data): self
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @return void
     */
    #[NoReturn] public function respond(): void
    {
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($this->data);
        exit;
    }
}