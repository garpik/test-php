<?php

namespace App\Interfaces;

interface ResponseInterface
{
    /**
     * @return void
     */
    public function respond(): void;
}