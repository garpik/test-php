<?php

namespace App\Interfaces;
interface DataObjectInterface
{

    public function __construct(array $data = []);

    /**
     * @param string|null $key
     * @return array|string
     */
    public function getData(?string $key = null): array|string;

    /**
     * @param string $key
     * @param string|array $value
     * @return self
     */
    public function setData(string $key = '', string|array $value = ''): self;

}