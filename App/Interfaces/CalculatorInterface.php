<?php

namespace App\Interfaces;

interface CalculatorInterface
{
    const string TABLE_NAME = 'transactions';

    /**
     * @param int $id
     * @return float
     */
    public function calculateById(int $id): float;
}