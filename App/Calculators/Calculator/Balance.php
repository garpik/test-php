<?php

namespace App\Calculators\Calculator;

use App\Calculators\AbstractCalculator;
use App\Exceptions\NoBalanceException;
use App\Provider\Mysql;

class Balance extends AbstractCalculator
{

    /**
     * @param Mysql|null $dbConnection
     */
    public function __construct(private ?Mysql $dbConnection = null)
    {
        if ($this->dbConnection === null) {
            $this->dbConnection = Mysql::getInstance();
        }
    }

    /**
     * @param int $id
     * @return float
     * @throws NoBalanceException
     */
    public function calculateById(int $id): float
    {
        $resultData = $this->dbConnection->execute(
            query: "SELECT 
                    sum(case when paid_by=:customerId then -amount else amount end) as sum
                    from " . $this->getTable() . " 
                    where paid_to=:customerId or paid_by=:customerId",
            bind: [
                "customerId" => $id
            ]
        );
        $sumObject = $resultData->fetch();
        if ($sumObject['sum'] === null) {
            throw new NoBalanceException(sprintf("Can't find customer with %d", $id));
        }
        return $sumObject['sum'];
    }
}