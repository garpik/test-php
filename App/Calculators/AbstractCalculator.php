<?php

namespace App\Calculators;

use App\Interfaces\CalculatorInterface;

/**
 *
 */
abstract class AbstractCalculator implements CalculatorInterface
{

    /**
     * @return string
     */
    protected function getTable(): string
    {
        return self::TABLE_NAME;
    }

}