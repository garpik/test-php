<?php

namespace App\Requests;

use App\BaseModels\DataObject;

class GetRequestReader extends DataObject
{

    private static $instance = null;

    public function __construct(array $data = [])
    {
        parent::__construct($data);
        $this->readGetRequest();
    }

    private function readGetRequest()
    {
        $this->data = $_REQUEST;
    }

    static public function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new static();
        }
        return self::$instance;
    }

}