<?php

namespace App\BaseModels;

use App\Interfaces\DataObjectInterface;
use Exception;

abstract class DataObject implements DataObjectInterface
{

    public function __construct(protected array $data = [])
    {
    }

    /**
     * @param $method
     * @param $args
     * @return $this|bool|mixed|null
     * @throws Exception
     */
    public function __call($method, $args)
    {
        $key = $this->underscore(substr($method, 3));
        switch (substr($method, 0, 3)) {
            case 'get':
                return $this->data[$key];
            case 'set':
                $value = isset($args[0]) ? $args[0] : null;
                return $this->data[$key] = $value;
            case 'uns':
                unset($this->data[$key]);
                return $this;
            case 'has':
                return isset($this->data[$key]);
        }
        throw new Exception("Can't get config parameter");
    }

    /**
     * @param string|null $key
     * @return array|string
     */
    public function getData(?string $key = null): array|string
    {
        if ($key === null) {
            return $this->data;
        }
        return $this->data[$key];
    }

    /**
     * @param string $key
     * @param string|array $value
     * @return $this
     */
    public function setData(string $key = '', string|array $value = ''): self
    {
        if (is_array($value)) {
            $this->data = $value;
            return $this;
        }
        $this->data[$key] = $value;
        return $this;
    }

    /**
     * @param $param
     * @return string
     */
    private function underscore($param): string
    {

        if (empty($param)) {
            return $param;
        }
        $param = lcfirst($param);
        $param = preg_replace("/[A-Z]/", "_$0", $param);
        return strtolower($param);
    }
}