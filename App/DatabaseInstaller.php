<?php

namespace App;

use App\Config\EnvReader;
use App\Provider\Mysql;

class DatabaseInstaller
{
    private const INSTALLED_SCRIPTS_CONFIG_FILE = 'installedScripts.json';
    private ?string $scriptsFolder = null;
    private array $installedScriptsArray = [];
    private string $currentFolder = '';

    /**
     * @param Provider\Mysql $dbProvider
     * @param Config\EnvReader $configProvider
     */
    public function __construct(
        protected Mysql     $dbProvider,
        protected EnvReader $configProvider
    )
    {
        $this->init();
    }

    /**
     * @return void
     */
    private function init(): void
    {
        $this->currentFolder = dirname(__FILE__);
        $this->scriptsFolder = $this->createPath(
            $this->currentFolder,
            '..',
            $this->configProvider->getInstallScriptFolder()
        );
        $this->loadInstalledScriptsConfig();
    }

    /**
     * @return void
     */
    private function loadInstalledScriptsConfig(): void
    {
        try {
            $configFileContent = file_get_contents(
                $this->createPath(
                    $this->currentFolder,
                    '..',
                    self::INSTALLED_SCRIPTS_CONFIG_FILE
                )
            );
            $this->installedScriptsArray = json_decode($configFileContent, true);
        } catch (\Exception $e) {
            $this->installedScriptsArray = [];
        }
    }

    /**
     * @return void
     */
    private function saveInstalled(): void
    {
        try {
            $configFilePath = $this->createPath(
                $this->currentFolder,
                '..',
                self::INSTALLED_SCRIPTS_CONFIG_FILE
            );
            file_put_contents($configFilePath, json_encode($this->installedScriptsArray));
        } catch (\Exception $e) {
        }
    }

    /**
     * @param string $moduleName
     * @return void
     */
    private function setInstalledModule(string $moduleName): void
    {
        $this->installedScriptsArray[$moduleName] = 1;
    }

    /**
     * @param string $moduleName
     * @return bool
     */
    private function isInstalled(string $moduleName): bool
    {
        return !empty($this->installedScriptsArray[$moduleName]);
    }

    /**
     * @return bool
     */
    public function install(): bool
    {
        if ($this->scriptsFolder === null) {
            return false;
        }
        foreach (scandir($this->scriptsFolder) as $file) {
            if (!$this->isFileIsCorrect($file)) {
                continue;
            }
            if ($this->isInstalled($file)) {
                continue;
            }
            try {
                $sqlData = file_get_contents($this->createPath($this->scriptsFolder, $file));
                $this->dbProvider->execute($sqlData);
                $this->setInstalledModule($file);
                $this->saveInstalled();
            } catch (\Exception $e) {
                $this->saveInstalled();
                return false;
            }
        }
        return true;
    }


    /**
     * @param string $path
     * @return bool
     */
    private function isFileIsCorrect(string $path): bool
    {
        return !in_array(
            $path,
            [
                '.',
                '..'
            ]
        );
    }

    /**
     * @param ...$pieces
     * @return string
     */
    private function createPath(...$pieces): string
    {
        $resultPath = '';
        foreach ($pieces as $piece) {
            $resultPath .= $piece . DIRECTORY_SEPARATOR;
        }
        return rtrim($resultPath, DIRECTORY_SEPARATOR);
    }
}