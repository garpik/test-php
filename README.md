Запуск проекта осуществляется через docker-compose файл:

```
docker-compose up -d --build
```
init скрипты выполняются из контейнера. Для этого стоит зайти в контейнер и запустить файл initMysql.php

```
docker exec -ti phpdummy_php_1 /bin/bash
php initMysql.php
```