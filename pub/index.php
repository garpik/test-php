<?php

use App\Calculators\Calculator\Balance;
use App\Config\EnvReader;
use App\Exceptions\NoBalanceException;
use App\Provider\Mysql;
use App\Requests\GetRequestReader;
use App\Response\Json;

require_once "../autoloader.php";

Autoloader::register();
$configProvider = new EnvReader(configFilePath: '../.env');
$connection = Mysql::initMysqlConnection($configProvider);

$jsonResponse = new Json();

$requestReader = GetRequestReader::getInstance();
try {
    $userId = $requestReader->getUserId();
    if (filter_var($userId, FILTER_VALIDATE_INT) === false) {
        throw new \Exception("UserID is not integer");
    }
    $calculator = new Balance();
    $jsonResponse->setData([
        'balance' => $calculator->calculateById($userId),
        'success' => true
    ]);
} catch (NoBalanceException $e) {
    $jsonResponse->setData([
        'message' => $e->getMessage(),
        'success' => false
    ]);
} catch (\Exception $e) {
    $jsonResponse->setData([
        'message' => "Error during process your request",
        'success' => false
    ]);
}

$jsonResponse->respond();